const express = require('express')
const next = require('next')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare()
  .then(() => {
    const server = express()

    server.get('/test/:page', (req, res) => {
      const page = req.params.page;
      let file = '';
      switch(page) {
         case 'page1':
               file = '/page1';
               break;
         case 'page2':
               file = '/page2';
               break;
         default:
               file = '/page1';
      }
      return app.render(req, res, file, { page })
    })

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    server.listen(port, (err) => {
      if (err) throw err
      console.log(`> Ready on http://localhost:${port}`)
    })
  })