import Link from 'next/link'

const linkStyle = {
  marginRight: 15
}

const Header = () => (
    <div>
        <Link href="/">
          <a style={linkStyle}>Home</a>
        </Link>
        <Link href="/test/page1">
          <a style={linkStyle}>page1 (reloads on click)</a>
        </Link>
        <Link href="/test/page2">
          <a style={linkStyle}>page2 (reloads on click)</a>
        </Link>
    </div>
)

export default Header
